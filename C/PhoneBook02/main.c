/**
    Implementarea unui meniu cu for. Acest cod
        este adaugat in Templates.
    @author: Mihai Cornel  mhcrnl@gmail.com
    @file: mainfor.c
*/
#include <stdio.h>
#include <stdlib.h>

void runProgram(){
    /** Declararea variabilelor */
    short selection =0;
    /** Implementarea unui meniu cu for */
    for( ; ; ){
        printf("1. - Adauga contact.\n"
                "2. - Sterge contact.\n"
                "3. - Cauta contact.\n"
                "4. - Afiseaza contact.\n"
                "5. - GitHub send code.\n"
                "6. - Exit.\n\n"
                "Selectati o optiune[1-6]: ");
        scanf("%d", &selection);
        switch(selection){
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                /** Trimite codul din acest fisier pe GITHUB */
                system("./gitpush.sh");
                break;
            case 6:
                printf("Multumesc ca a-ti ales acest program.\n");
               exit(0);
            default:
                printf("Selectia dvs este invalida ...\n");
               break;
        }
        printf("\n");
    }
}

int main(){

    runProgram();

    return 0;
}

