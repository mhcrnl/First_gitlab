#include <stdio.h>
#include <stdlib.h>
#include <string.h>



int main()
{
    printf("Hello world!Alocaarea dinamica a memoriei in c.\n");

    char nume[30];
    char *adresa;

    strcpy(nume, "Mihai Cornel");
    // Alocarea dinamica a memoriei
    adresa = malloc(30 * sizeof(char));

    if(adresa == NULL){
        fprintf(stderr, "Memoria nu a putut fi alocata.\n");
    } else {
        strcpy(adresa,"Prevederii 23, Bucuresti, Sector 3." );
    }
    // Realocarea de memorie in mod dinamic
    adresa = realloc(adresa, 100*sizeof(char));
    if(adresa == NULL){
        fprintf(stderr, "EROARE-memoria nu a fost alocata.\n");
    } else {
        strcat(adresa, "Bl G16, sc B, ap 45");
    }

    fprintf(stdout, "%s\n", nume);
    fprintf(stdout, "%s", adresa);

    free(adresa);

    return 0;
}
