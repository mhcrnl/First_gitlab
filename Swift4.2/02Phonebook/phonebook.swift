//import Fundation

class PhoneBook {
    var nume:String
    var prenume:String
    var nrTelefon:String
    // Designated initializer
    init(nume:String, prenume:String, nrTelefon:String) {
        self.nume = nume
        self.prenume = prenume
        self.nrTelefon = nrTelefon

    }
    func printPhoneBook() -> Void {
        print("Nume: \(self.nume)")
        print("Prenume: \(self.prenume)")
        print("Numar Telefon: \(self.nrTelefon)")
    }
}

let contact = PhoneBook(nume:"Mihai", prenume:"Cornel", nrTelefon:"0723455768")
print(contact.nume, contact.prenume, contact.nrTelefon)
contact.printPhoneBook()

func meniu() {
    let ArrayName:[String] = ["1.Adauga", "2.Afiseaza", "3.Inchideti"]
    for each in ArrayName {
        print(each)
    } 
}

meniu()
