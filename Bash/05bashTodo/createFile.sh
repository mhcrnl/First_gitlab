#!/bin/sh

# Author : Mihai Cornel
# Copyright (c) mhcrnl@gmail.com
# Version 0.00.3
# Create a file in bash script si deschiderea acesteia in vim.

BASH_FILE=("VERSION: 00.00.1" "AUTHOR: Mihai Cornel mhcrnl@gmail")
HELP="Acest script creaza o fila a carei denumire si extensie este adaugata 
de utilizator"
DATA=`date`
################################################################
usage()
{
    cat << EOF
FILE CREATOR
Creaza o fila care este introdusa de utilizator. 
EOF
}
###############################################################
help(){
    echo ${HELP}
    echo ${BASH_FILE[@]}
}
#####################################################################
# Functia creaza o fila a carei denumire este introdusa de utilizator'
createFile(){
    help
    usage
    echo $PWD
    echo "Introduceti numele filei si extensia (Ex: script.sh): "
    read numefila

    echo "#!/bin/sh" >> ${numefila}
    echo "# ${BASH_FILE[0]}" >>  ${numefila}
    echo "# ${BASH_FILE[1]}" >> ${numefila}
    echo "# FILE: `pwd` "  >> ${numefila} 
    echo "#  ${DATA}" >> ${numefila}

    vim ${numefila}
   
   # send email to mhcrnl@gmail.
  $mail -s "Fila a fost creata" mhcrnl@gmail.com < ${numefila}

 
}
##################################################################
main_menu()
{
 while :
 do
     clear
     echo "---------------------------------------------------------"
     echo "==================== MAIN MENU =========================="
     echo "---------------------------------------------------------"
     echo "[1] Show todays date/time"
     echo "[2] Show files in current directory"
     echo "[3] Show calendar"
     echo "[4] Create a file and start vim editor"
     echo "[5] Exit/Stop"
     echo "========================================================="
     echo -n "Enter your menu choise [1-5]: "
     read yourch
     case $yourch in
         1) echo "Today is `date` , press a key. . ."; read ;;
         2) echo "Files in `pwd`" ; ls -l ; echo "Press a key. . ."; read ;;
         3) cal ; echo "Press a key. . ." ; read ;;
         4) createFile ; echo "Press a key. . ." ; read ;;
         5) exit 0 ;;
         *) echo "Opps!!! Please select choise 1,2,3,4, or 5." ;
             echo "Press a key. . ." ; read ;;
     esac
 done
}
# Aici incepe executia filei bash
main_menu

