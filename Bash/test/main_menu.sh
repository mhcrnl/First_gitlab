#!/bin/sh
# VERSION 00.00.1
# AUTHOR: Mihai Cornel mhcrnl@gmail
# Sun Jul  8 08:15:39 EEST 2018
####################################################################
# Script to create simple menus and take action according to that selected
# menu item.
####################################################################
while :
do
    clear
    echo "----------------------------------------------------------"
    echo "====================== MAIN MENU ========================="
    echo "----------------------------------------------------------"
    echo "[1] Show todays date/time"
    echo "[2] Show files in current directory"
    echo "[3] Show calendar"
    echo "[4] Start editor to write letters"
    echo "[5] Exit/Stop"
    echo "=========================================================="
    echo -n "Enter your menu choise [1-5]: "
    read yourch
    case $yourch in
        1) echo "Today is `date` , press a key. . ."; read ;;
        2) echo "Files in `pwd`" ; ls -l ; echo "Press a key..." ; read ;;
        3) cal ; echo "Press a key. . ." ; read ;;
        4) vim ;;
        5) exit 0 ;;
        *) echo "Opps!!! Please select choise 1,2,3,4, or 4";
           echo "Press a key. . ." ; read ;;
   esac
done

