#!/bin/sh

# Creaza o fila README.md si o deschide cu programul ReText - editor markdown care trebuie sa fie instalat

VERSION="VERSION:0.00.1"
AUTHOR="AUTHOR:Mihai Cornel mhcrnl@gmail."
FILE="README.md"

createReadme()
{
   echo "## Title" >> ${FILE}
   echo " " >> ${FILE}
   echo "\`\`\`$ git clone ..." >> ${FILE}
   echo "$ cd ----" >> ${FILE}
   echo "./.......\`\`\`" >> ${FILE}
   echo " " >> ${FILE}
   echo "![img01.png](img01.png)" >>${FILE}
   retext ${FILE}
}

createReadme

