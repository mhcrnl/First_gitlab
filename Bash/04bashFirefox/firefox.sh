#!/bin/sh
# VERSION 00.00.1
# AUTHOR: Mihai Cornel mhcrnl@gmail
# Sun Jul  8 07:25:25 EEST 2018

# Deschide o fereastra firefox cu un tutorial bash

firefox http://www.freeos.com/guides/lsst/ch02sec15.html http://tldp.org/LDP/abs/html/index.html https://www.gnu.org/software/bash/manual/bash.html#What-is-Bash_003f http://tille.garrels.be/training/bash/ https://www.ossblog.org/wp-content/uploads/2017/06/Linux_shell_scripting_v2.pdf


